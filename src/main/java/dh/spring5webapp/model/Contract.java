package dh.spring5webapp.model;

import javax.persistence.OneToOne;
import java.util.Date;

public class Contract extends ModelBase {
    private Employee employee;
    private Position position;
    private Date initDate;
    private Date endDate;

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public void setInitDate(Date initDate) {
        this.initDate = initDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Employee getEmployee() {
        return employee;
    }

    public Position getPosition() {
        return position;
    }

    public Date getInitDate() {
        return initDate;
    }

    public Date getEndDate() {
        return endDate;
    }
}
