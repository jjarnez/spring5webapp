package dh.spring5webapp.model;

public class Position extends ModelBase {
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
