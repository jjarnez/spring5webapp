package dh.spring5webapp.model;

import javax.persistence.OneToOne;

public class SubCategory extends ModelBase {
    private String name;
    private String code;

    private Category category;

    public void setName(String n) {
        name = n;
    }

    public void setCode(String c) {
        code = c;
    }

    public void setCategory(Category c) {
        category = c;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public Category getCategory() {
        return category;
    }

}
